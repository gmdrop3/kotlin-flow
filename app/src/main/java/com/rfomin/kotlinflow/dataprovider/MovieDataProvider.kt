package com.rfomin.kotlinflow.dataprovider

import com.rfomin.kotlinflow.App
import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.mapper.toMovieModelList
import com.rfomin.kotlinflow.result.DataResult
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.flow.*


@FlowPreview
@ExperimentalCoroutinesApi
class MovieDataProvider: DataProvider{
    private val api = App.getDaggerAppComponent().provideApi()
    private val database = App.getDaggerAppComponent().provideDataBase()

    private val searchStringChannel = ConflatedBroadcastChannel<String>()

    override fun getMovieList(searchString: String) {
        searchStringChannel.offer(searchString)
    }

    private fun loadFromApiInBackground() {
        searchStringChannel.asFlow().onEach { searchString ->
            val newMovieList = when {
                searchString.isEmpty() -> api.getPopularMovies().toMovieModelList()
                else -> api.getSearchMovie(searchString).toMovieModelList()
            }
            database.getMovieDao().insert(newMovieList)
        }.launchIn(GlobalScope)
    }

    override fun getMovieListRoomFlow(): Flow<DataResult<List<MovieModel>>> {
        loadFromApiInBackground()

        return searchStringChannel
            .asFlow()
            .flatMapLatest { searchString ->
                when {
                    searchString.isEmpty() -> allMoviesFlow()
                    else -> filteredMoviesFlow(searchString)
                }
            }.map {movieModelList -> //List<MovieModel>
                DataResult.Success.Cached(movieModelList)
            }
    }

    private fun allMoviesFlow():Flow<List<MovieModel>> {
        return database.getMovieDao().getAllMovieListFlow()
    }
    private fun filteredMoviesFlow(searchString: String):Flow<List<MovieModel>> {
        return database.getMovieDao().getSearchMovieListFlow("%$searchString%")
    }

    override fun getMovieListNetworkFlow(): Flow<DataResult<List<MovieModel>>> {
        return flow {
            searchStringChannel.openSubscription().consumeEach {searchString ->
                emit(DataResult.LoadingStart)

                try {
                    val cachedMovieList = when {
                        searchString.isEmpty() -> database.getMovieDao()
                            .getAllMovieList()
                        else -> database.getMovieDao()
                            .getSearchMovieList("%$searchString%")
                    }
                    emit(DataResult.Success.Cached(cachedMovieList))

                    val newMovieList = when {
                        searchString.isEmpty() -> api.getPopularMovies().toMovieModelList()
                        else -> api.getSearchMovie(searchString).toMovieModelList()
                    }
                    emit(DataResult.Success.Fresh(newMovieList))
                    database.getMovieDao().insert(newMovieList)
                } catch (e: Exception) {
                    emit(DataResult.Error(e))
                }
            }
        }
    }

    override fun getMovieListFlowWithErrors(): Flow<DataResult<List<MovieModel>>> = flow {
        searchStringChannel.openSubscription().consumeEach {searchString ->
            for (i in 0 until 3) {
                emit(DataResult.Error(Exception("Fake error for \"$searchString\""), i+1))
                delay(2000)
            }

            try {
                val newMovieList = when {
                    searchString.isEmpty() -> api.getPopularMovies().toMovieModelList()
                    else -> api.getSearchMovie(searchString).toMovieModelList()
                }
                emit(DataResult.Success.Fresh(newMovieList))
            } catch (e: Exception) {
                emit(DataResult.Error(e))
            }
        }
    }
}
