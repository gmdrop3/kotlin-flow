package com.rfomin.kotlinflow.dataprovider

import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.result.DataResult
import kotlinx.coroutines.flow.Flow

interface DataProvider {
    fun getMovieList(searchString: String)

    fun getMovieListNetworkFlow(): Flow<DataResult<List<MovieModel>>>
    fun getMovieListRoomFlow(): Flow<DataResult<List<MovieModel>>>
    fun getMovieListFlowWithErrors(): Flow<DataResult<List<MovieModel>>>
}