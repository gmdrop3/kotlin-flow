package com.example.myapplication.di

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule {

        @Provides
        fun provideContext(): Context {
            return ContextProvider.getContext()
        }

}