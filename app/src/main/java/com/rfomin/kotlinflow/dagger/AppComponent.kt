package com.rfomin.kotlinflow.dagger

import com.example.myapplication.di.ContextModule
import com.rfomin.kotlinflow.api.ApiFactory
import com.rfomin.kotlinflow.api.TmdbApi
import com.rfomin.kotlinflow.dagger.provider.DaggerProvider
import com.rfomin.kotlinflow.database.MoviesDatabase
import com.rfomin.kotlinflow.dataprovider.DataProvider
import dagger.Component
import javax.inject.Singleton

@Component(modules = [ApiFactory::class
    , DaggerProvider::class
    , ContextModule::class])
@Singleton
interface AppComponent {
    fun provideApi(): TmdbApi
    fun provideApiFactory(): ApiFactory
    fun provideDataProvider(): DataProvider

    fun provideDataBase(): MoviesDatabase
}