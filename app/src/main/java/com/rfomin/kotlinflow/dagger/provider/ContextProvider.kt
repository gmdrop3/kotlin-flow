package com.example.myapplication.di

import android.content.Context

object ContextProvider {

    private lateinit var context: Context

    fun setContext(context: Context) {
        this.context = context
    }

    fun getContext(): Context {
        return this.context
    }
}