package com.rfomin.kotlinflow.api

import com.rfomin.kotlinflow.model.TmdbMovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface TmdbApi {
    @GET("movie/popular")
    suspend fun getPopularMovies() : TmdbMovieResponse

    @GET("search/movie")
    suspend fun getSearchMovie(@Query("query") searchString: String) : TmdbMovieResponse
}
