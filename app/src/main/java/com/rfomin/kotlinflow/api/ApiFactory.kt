package com.rfomin.kotlinflow.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rfomin.kotlinflow.BuildConfig
import com.rfomin.kotlinflow.DemoConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiFactory {
    companion object {
        const val IMAGE_URL = "https://image.tmdb.org/t/p/w500"
        private const val VALID_URL = "https://api.themoviedb.org/3/"
        private const val INVALID_URL = "https://invalid.url"

        private var BASE_URL = if (DemoConfig.LOAD_MOVIES_FROM_THE_INTERNET) { VALID_URL }
            else { INVALID_URL }
    }


    @Provides
    internal fun provideInterceptor() = Interceptor {
        val newUrl = it.request().url()
            .newBuilder()
            .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
            .build()

        val newRequest = it.request()
            .newBuilder()
            .url(newUrl)
            .build()

        it.proceed(newRequest)
    }

    @Provides
    @Singleton
    internal fun provideHttpClient(authInterceptor: Interceptor): OkHttpClient {
        return OkHttpClient().newBuilder()
            .addInterceptor(authInterceptor)
            .build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideTMDBApi(retrofit: Retrofit): TmdbApi = retrofit.create(TmdbApi::class.java)

}