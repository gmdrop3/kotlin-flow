package com.rfomin.kotlinflow.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.rfomin.kotlinflow.R
import com.rfomin.kotlinflow.api.ApiFactory.Companion.IMAGE_URL
import com.rfomin.kotlinflow.database.model.EmptyMovie
import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.viewmodel.MovieViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_details_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.lang.Exception

@FlowPreview
@ExperimentalCoroutinesApi
class MovieDetailsFragment : Fragment() {
    private lateinit var viewModel: MovieViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_details_fragment, container , false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideDetails()
    }

    private fun setMovieDetails(movieDetails: MovieModel) {
        no_details.visibility = View.GONE
        scroll_view.visibility = View.VISIBLE
        movie_name.text = movieDetails.title
        movie_description.text = movieDetails.overview
        movie_rating.text = movieDetails.vote_average.toString()
        scroll_view.scrollTo(0, 0)
        progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load("$IMAGE_URL${movieDetails.poster_path}")
            .centerInside()
            .fit()
            .into(image, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                    image.setImageResource(R.drawable.no_image_available)
                }
            })
    }

    private fun hideDetails() {
        scroll_view.visibility = View.GONE
        no_details.visibility = View.VISIBLE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val fragmentActivity = requireActivity()
        viewModel = ViewModelProvider(fragmentActivity).get(MovieViewModel::class.java)
        viewModel.getSelectedMovieLiveData().observe(viewLifecycleOwner, Observer{ movie ->
            when (movie) {
                is EmptyMovie -> hideDetails()
                is MovieModel -> setMovieDetails(movie)
            }
        })

    }
}
