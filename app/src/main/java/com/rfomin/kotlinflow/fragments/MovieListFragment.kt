package com.rfomin.kotlinflow.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.rfomin.kotlinflow.R
import com.rfomin.kotlinflow.adapters.MovieListAdapter
import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.result.DataResult
import com.rfomin.kotlinflow.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@ExperimentalCoroutinesApi
@FlowPreview
class MovieListFragment : Fragment() {
    private var listener: MovieListInteractionListener? = null
    private lateinit var viewModel: MovieViewModel

    private var listClickListener: (MovieModel) -> Unit = {}
    private var hasMoviesResponse = false
    private var searchMovieSource = SearchMovieSource.NONE

    private enum class SearchMovieSource{
        NONE,
        NETWORK,
        CACHE,
        ERRORS
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_movie.addTextChangedListener( object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(charSequence: CharSequence, p1: Int, p2: Int, p3: Int) {
                searchMovieSource = SearchMovieSource.NETWORK
                val text: String = charSequence.toString()
                if (text.isEmpty() || text.length >= 3) {
                    viewModel.searchMovie(text)
                }
            }
        })

        search_movie_2.addTextChangedListener( object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                searchMovieSource = SearchMovieSource.CACHE
                val text: String = charSequence?.toString() ?: ""
                if (text.isEmpty() || text.length >= 3) {
                    viewModel.searchMovie(text)
                }
            }
        })

        search_movie_3.addTextChangedListener( object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                searchMovieSource = SearchMovieSource.ERRORS
                val text: String = charSequence?.toString() ?: ""
                if (text.isEmpty() || text.length >= 3) {
                    viewModel.searchMovie(text)
                }
            }
        })
    }

    private fun setMovies(movies: List<MovieModel>, isDataFresh: Boolean = false) {
        if (movies.isNotEmpty()) {
            recyclerview_list.adapter = MovieListAdapter(movies, listClickListener)
            recyclerview_list.visibility = View.VISIBLE
            no_items.visibility = View.GONE
        } else {
            recyclerview_list.visibility = View.GONE
            if (isDataFresh) {
                no_items.visibility = View.VISIBLE
            } else {
                no_items.visibility = View.GONE
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val fragmentActivity = requireActivity()
        viewModel = ViewModelProvider(fragmentActivity).get(MovieViewModel::class.java)

        listClickListener = {movieModel ->
            viewModel.onListItemClick(movieModel)
            listener?.onItemClick()
        }

        searchMovieSource = SearchMovieSource.CACHE

        val networkObserver = Observer<DataResult<List<MovieModel>>> { dataResult ->
            processDataResultForSource(dataResult, SearchMovieSource.NETWORK)
        }

        val cacheObserver = Observer<DataResult<List<MovieModel>>> { dataResult ->
            processDataResultForSource(dataResult, SearchMovieSource.CACHE)
        }

        val errorsObserver =  Observer<DataResult<List<MovieModel>>> { dataResult ->
            processDataResultForSource(dataResult, SearchMovieSource.ERRORS)
        }

        viewModel.getNetworkMoviesLiveData().observe(viewLifecycleOwner, networkObserver)
        viewModel.getRoomMoviesLiveData().observe(viewLifecycleOwner, cacheObserver)
        viewModel.getMoviesWithErrorsLiveData().observe(viewLifecycleOwner, errorsObserver)
    }

    private fun processDataResultForSource(dataResult: DataResult<List<MovieModel>>, movieSource: SearchMovieSource) {
        if (movieSource != searchMovieSource) {
            return
        }
        when (dataResult) {
            is DataResult.LoadingStart -> {
                if (hasMoviesResponse) {
                    recyclerview_list.visibility = View.VISIBLE
                } else {
                    recyclerview_list.visibility = View.GONE
                }
                no_items.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            }
            is DataResult.Success -> {
                hasMoviesResponse = dataResult.data.isNotEmpty()
                setMovies(dataResult.data, dataResult is DataResult.Success.Fresh)
                progress_bar.visibility = View.GONE
                error.visibility = View.GONE
            }
            is DataResult.Error -> {
                recyclerview_list.visibility = View.GONE
                no_items.visibility = View.GONE
                progress_bar.visibility = View.GONE
                error.visibility = View.VISIBLE

                val errorText = if (dataResult.attempts > 0) {
                    "${dataResult.exception}. Attempt ${dataResult.attempts}"
                } else {
                    dataResult.exception.toString()
                }
                Snackbar.make(coordinatorLayout, errorText, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MovieListInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface MovieListInteractionListener {
        fun onItemClick()
    }
}
