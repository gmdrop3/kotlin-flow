package com.rfomin.kotlinflow.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rfomin.kotlinflow.R
import com.rfomin.kotlinflow.database.model.MovieModel
import com.squareup.picasso.Callback

import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.movie_item.view.*
import java.lang.Exception

class MovieListAdapter(
    private val values: List<MovieModel>,
    private val itemClickListener: (MovieModel) -> Unit = {}
) : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as MovieModel
            itemClickListener(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.mIdView.text = item.title
        holder.mContentView.text = item.overview
        holder.mRatingView.text = item.vote_average.toString()
        Picasso.get()
            .load("https://image.tmdb.org/t/p/w500" + item.poster_path)
            .centerInside()
            .fit()
            .into(holder.mImageView, object : Callback {
                override fun onSuccess() {}
                override fun onError(e: Exception?) {
                    holder.mImageView.setImageResource(R.drawable.no_image_available)
                }
            })

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.title
        val mContentView: TextView = mView.description
        val mRatingView: TextView = mView.rating
        val mImageView: ImageView = mView.image

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }

    interface ListItemClickListener {
        fun onItemClicked(item: MovieModel)
    }
}
