package com.rfomin.kotlinflow.database.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.rfomin.kotlinflow.database.model.MovieModel
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {
    @Query("SELECT * FROM MovieModel")
    suspend fun getAllMovieList(): List<MovieModel>

    @Query("SELECT * FROM MovieModel WHERE title LIKE :searchString ORDER BY title")
    suspend fun getSearchMovieList(searchString: String): List<MovieModel>

    @Query("SELECT * FROM MovieModel")
    fun getAllMovieListFlow(): Flow<List<MovieModel>>

    @Query("SELECT * FROM MovieModel WHERE title LIKE :searchString ORDER BY title")
    fun getSearchMovieListFlow(searchString: String): Flow<List<MovieModel>>

    @Insert(onConflict = REPLACE)
    suspend fun insert(movieList: List<MovieModel>)

    @Delete
    suspend fun delete(movieList: List<MovieModel>)

    @Query("DELETE FROM MovieModel")
    suspend fun deleteAll()

    @Update
    suspend fun update(movieList: List<MovieModel>)
}