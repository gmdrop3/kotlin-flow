package com.rfomin.kotlinflow.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rfomin.kotlinflow.database.dao.MovieDao
import com.rfomin.kotlinflow.database.model.MovieModel

@Database(entities = [MovieModel::class], version = 1, exportSchema = false)
abstract class MoviesDatabase : RoomDatabase() {
    abstract fun getMovieDao() : MovieDao
}