package com.rfomin.kotlinflow

import android.app.Application
import com.example.myapplication.di.ContextProvider
import com.rfomin.kotlinflow.dagger.AppComponent
import com.rfomin.kotlinflow.dagger.DaggerAppComponent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class App: Application() {
    companion object {
        private lateinit var daggerAppComponent: AppComponent
        fun getDaggerAppComponent() = daggerAppComponent
    }

    override fun onCreate() {
        super.onCreate()
        ContextProvider.setContext(this)

        daggerAppComponent = DaggerAppComponent.builder().build()
        if (DemoConfig.DELETE_MOVIES_FROM_DATABASE_ON_APP_START) {
            GlobalScope.launch {
                daggerAppComponent.provideDataBase().getMovieDao().deleteAll()
            }
        }
    }
}