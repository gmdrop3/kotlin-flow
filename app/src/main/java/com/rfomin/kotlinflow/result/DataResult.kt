package com.rfomin.kotlinflow.result

sealed class DataResult<out T> {
    object LoadingStart : DataResult<Nothing>()
    sealed class Success<out T>(val data: T) : DataResult<T>() {
        class Cached<out T>(data: T): Success<T>(data)
        class Fresh<out T>(data: T): Success<T>(data)
    }
    data class Error(val exception: Exception?, val attempts: Int = 0) : DataResult<Nothing>()
}
