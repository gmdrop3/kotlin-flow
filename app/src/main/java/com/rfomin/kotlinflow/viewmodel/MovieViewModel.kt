package com.rfomin.kotlinflow.viewmodel

import androidx.lifecycle.*
import com.rfomin.kotlinflow.App
import com.rfomin.kotlinflow.database.model.EmptyMovie
import com.rfomin.kotlinflow.database.model.Movie
import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.dataprovider.DataProvider
import com.rfomin.kotlinflow.result.DataResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


@FlowPreview
@ExperimentalCoroutinesApi
class MovieViewModel : ViewModel() {
    private val dataProvider: DataProvider = App.getDaggerAppComponent().provideDataProvider()

    private val selectedMovieMutableLiveData = MutableLiveData<Movie>()
    fun getSelectedMovieLiveData() = selectedMovieMutableLiveData

    init {
        selectedMovieMutableLiveData.value = EmptyMovie.model()
        viewModelScope.launch {
            searchMovie("")
        }
    }

    fun searchMovie(searchString: String) {
        dataProvider.getMovieList(searchString)
    }

    fun onListItemClick(itemData: Movie) {
        selectedMovieMutableLiveData.value = itemData
    }

    private fun updateSelection(movies: List<MovieModel>) {
        val selectedMovieId = (selectedMovieMutableLiveData.value as? MovieModel)?.id ?: 0
        val movie = movies.find { it.id == selectedMovieId }
        if (movie == null) {
            selectedMovieMutableLiveData.postValue(EmptyMovie.model())
        }
    }

    fun getNetworkMoviesLiveData(): LiveData<DataResult<List<MovieModel>>> =
        dataProvider.getMovieListNetworkFlow().toMovieListLiveData()

    fun getRoomMoviesLiveData(): LiveData<DataResult<List<MovieModel>>> =
        dataProvider.getMovieListRoomFlow().toMovieListLiveData()

    fun getMoviesWithErrorsLiveData(): LiveData<DataResult<List<MovieModel>>> =
        dataProvider.getMovieListFlowWithErrors().toMovieListLiveData()

    private fun Flow<DataResult<List<MovieModel>>>.toMovieListLiveData() =
        this.onEach {
            if (it is DataResult.Success) {
                updateSelection(it.data)
            }
        }.flowOn(Dispatchers.IO).asLiveData()
}