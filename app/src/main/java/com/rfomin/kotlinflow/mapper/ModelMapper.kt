package com.rfomin.kotlinflow.mapper

import com.rfomin.kotlinflow.database.model.MovieModel
import com.rfomin.kotlinflow.model.TmdbMovieResponse

fun TmdbMovieResponse.toMovieModelList(): List<MovieModel> {
    return results.map { tmdbMovie ->
        MovieModel(
            id = tmdbMovie.id,
            vote_average = tmdbMovie.vote_average,
            title = tmdbMovie.title,
            overview = tmdbMovie.overview,
            adult = tmdbMovie.adult,
            poster_path = tmdbMovie.poster_path
        )
    }
}