# Kotlin Flow example #

In order to use this application you should have your own TMDB API key.
You can create it on [TMDB page](https://developers.themoviedb.org/3/getting-started/introduction)

after that add this key to your local.properties file:

tmdb_api_key="YOUR_TMDB_API_KEY" 
